package main

import (
	"fmt"
	"log"
	"os"

	days "gitlab.com/fpopa/AdventOfCode/2020/go/days"
)

func main() {
	arg := os.Args[1]

	if err := run(arg); err != nil {
		log.Printf("%s\n", err)
		os.Exit(1)
	}
}

func run(day string) error {
	daysFunctions := map[string]func() error{
		"1":  days.Day1,
		"2":  days.Day2,
		"3":  days.Day3,
		"4":  days.Day4,
		"5":  days.Day5,
		"6":  days.Day6,
		"7":  days.Day7,
		"8":  days.Day8,
		"9":  days.Day9,
		"10": days.Day10,
		"11": days.Day11,
	}

	if _, ok := daysFunctions[day]; !ok {
		return fmt.Errorf("Day %v not done yet", day)
	}

	return daysFunctions[day]()
}
