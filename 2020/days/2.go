package days

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type password struct {
	min    int
	max    int
	letter rune
	hash   string
}

// letter between min and max inclusive
func (p password) valid() bool {
	c := 0
	for _, l := range p.hash {
		if l != p.letter {
			continue
		}
		c++
	}

	return p.min <= c && c <= p.max
}

// letter present on exactly one of the min or max indexes
func (p password) validPart2() bool {
	parts := strings.Split(p.hash, "")

	minok := []rune(parts[p.min-1])[0] == p.letter
	maxok := []rune(parts[p.max-1])[0] == p.letter

	return (minok || maxok) && !(minok && maxok)
}

func getInputDay2() ([]password, error) {
	passwords := []password{}
	f, err := os.Open("inputs/2.txt")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		l := scanner.Text()
		parts := strings.Split(l, " ")
		min, _ := strconv.Atoi(strings.Split(parts[0], "-")[0])
		max, _ := strconv.Atoi(strings.Split(parts[0], "-")[1])
		letter := []rune(parts[1])[0]

		hash := parts[2]

		passwords = append(passwords, password{
			min:    min,
			max:    max,
			letter: letter,
			hash:   hash,
		})
	}

	err = scanner.Err()
	if err != nil {
		return nil, err
	}

	return passwords, nil
}

func validPasswordsCount(passwords []password) int {
	c := 0
	for _, p := range passwords {
		//if p.valid() {
		if p.validPart2() {
			c++
		}
	}

	return c
}

// Day2 implements day one challenge
func Day2() error {
	passwords, err := getInputDay2()
	if err != nil {
		return err
	}

	fmt.Println(validPasswordsCount(passwords))

	return nil
}

// https://adventofcode.com/2020/day/2
// --- Day 2: Password Philosophy ---
// Your flight departs in a few days from the coastal airport; the easiest way down to the coast from here is via toboggan.
//
// The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day. "Something's wrong with our computers; we can't log in!" You ask if you can take a look.
//
// Their password database seems to be a little corrupted: some of the passwords wouldn't have been allowed by the Official Toboggan Corporate Policy that was in effect when they were chosen.
//
// To try to debug the problem, they have created a list (your puzzle input) of passwords (according to the corrupted database) and the corporate policy when that password was set.
//
// For example, suppose you have the following list:
//
// 1-3 a: abcde
// 1-3 b: cdefg
// 2-9 c: ccccccccc
// Each line gives the password policy and then the password. The password policy indicates the lowest and highest number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password must contain a at least 1 time and at most 3 times.
//
// In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no instances of b, but needs at least 1. The first and third passwords are valid: they contain one a or nine c, both within the limits of their respective policies.
//
// How many passwords are valid according to their policies?
//
