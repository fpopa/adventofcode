package days

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func punchcardValidators() map[string]func(string) bool {
	return map[string]func(string) bool{
		// byr (Birth Year) - four digits; at least 1920 and at most 2002.
		"byr": func(v string) bool {
			year, _ := strconv.Atoi(v)
			return 1920 <= year && year <= 2002
		},
		// iyr (Issue Year) - four digits; at least 2010 and at most 2020.
		"iyr": func(v string) bool {
			year, _ := strconv.Atoi(v)
			return 2010 <= year && year <= 2020
		},
		// eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
		"eyr": func(v string) bool {
			year, _ := strconv.Atoi(v)
			return 2020 <= year && year <= 2030
		},
		// hgt (Height) - a number followed by either cm or in:
		// If cm, the number must be at least 150 and at most 193.
		// If in, the number must be at least 59 and at most 76.
		"hgt": func(v string) bool {
			unitType := string(v[len(v)-2:])
			value, _ := strconv.Atoi(string(v[:len(v)-2]))

			if unitType == "cm" {
				return 150 <= value && value <= 193
			}

			return 59 <= value && value <= 76
		},
		// hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
		"hcl": func(v string) bool {
			r := regexp.MustCompile(`^#[0-9,a-f]{6}$`)
			return r.MatchString(v)
		},
		// ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
		"ecl": func(v string) bool {
			switch v {
			case "amb":
				return true
			case "blu":
				return true
			case "brn":
				return true
			case "gry":
				return true
			case "grn":
				return true
			case "hzl":
				return true
			case "oth":
				return true
			default:
				return false
			}
		},
		// pid (Passport ID) - a nine-digit number, including leading zeroes.
		"pid": func(v string) bool {
			r := regexp.MustCompile(`^[0-9]{9}$`)
			return r.MatchString(v)
		},
		// cid (Country ID) - ignored, missing or not.
		"cid": func(_ string) bool {
			return true
		},
	}
}

func validPassport(passport string) int {
	passport = strings.TrimSpace(passport)
	punchcardValidators := punchcardValidators()
	punchcard := map[string]bool{
		"byr": false,
		"iyr": false,
		"eyr": false,
		"hgt": false,
		"hcl": false,
		"ecl": false,
		"pid": false,
		"cid": false,
	}
	for _, field := range strings.Split(passport, " ") {
		// part one
		// punchcard[strings.Split(field, ":")[0]] = true

		// part two
		parts := strings.Split(field, ":")
		name := parts[0]
		value := parts[1]
		punchcard[name] = punchcardValidators[name](value)
	}

	// count missing fields
	missingFields := 0
	for _, value := range punchcard {
		if value != true {
			missingFields++
			continue
		}
	}

	// accept one missing field only.
	if missingFields > 1 {
		return 0
	}

	// country Id is true and there is also a missing field => there's a required field missing -> invalidated.
	if punchcard["cid"] == true && missingFields == 1 {
		return 0
	}

	return 1
}

// Day4 implements day one challenge
func Day4() error {
	lines, err := readFile("inputs/4.txt")
	if err != nil {
		return err
	}

	validPassports := 0
	passport := ""
	for _, l := range lines {
		if l == "" {
			validPassports += validPassport(passport)
			passport = ""
			continue
		}

		passport += " " + l
	}
	validPassports += validPassport(passport)

	fmt.Println(validPassports)
	return nil
}

// https://adventofcode.com/2020/day/4
// --- Day 4: Passport Processing ---
// You arrive at the airport only to realize that you grabbed your North Pole Credentials instead of your passport. While these documents are extremely similar, North Pole Credentials aren't issued by a country and therefore aren't actually valid documentation for travel in most of the world.
//
// It seems like you're not the only one having problems, though; a very long line has formed for the automatic passport scanners, and the delay could upset your travel itinerary.
//
// Due to some questionable network security, you realize you might be able to solve both of these problems at the same time.
//
// The automatic passport scanners are slow because they're having trouble detecting which passports have all required fields. The expected fields are as follows:
//
// byr (Birth Year)
// iyr (Issue Year)
// eyr (Expiration Year)
// hgt (Height)
// hcl (Hair Color)
// ecl (Eye Color)
// pid (Passport ID)
// cid (Country ID)
// Passport data is validated in batch files (your puzzle input). Each passport is represented as a sequence of key:value pairs separated by spaces or newlines. Passports are separated by blank lines.
//
// Here is an example batch file containing four passports:
//
// ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
// byr:1937 iyr:2017 cid:147 hgt:183cm
//
// iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
// hcl:#cfa07d byr:1929
//
// hcl:#ae17e1 iyr:2013
// eyr:2024
// ecl:brn pid:760753108 byr:1931
// hgt:179cm
//
// hcl:#cfa07d eyr:2025 pid:166559648
// iyr:2011 ecl:brn hgt:59in
// The first passport is valid - all eight fields are present. The second passport is invalid - it is missing hgt (the Height field).
//
// The third passport is interesting; the only missing field is cid, so it looks like data from North Pole Credentials, not a passport at all! Surely, nobody would mind if you made the system temporarily ignore missing cid fields. Treat this "passport" as valid.
//
// The fourth passport is missing two fields, cid and byr. Missing cid is fine, but missing any other field is not, so this passport is invalid.
//
// According to the above rules, your improved system would report 2 valid passports.
//
// Count the number of valid passports - those that have all required fields. Treat cid as optional. In your batch file, how many passports are valid?

// byr (Birth Year) - four digits; at least 1920 and at most 2002.
// iyr (Issue Year) - four digits; at least 2010 and at most 2020.
// eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
// hgt (Height) - a number followed by either cm or in:
// If cm, the number must be at least 150 and at most 193.
// If in, the number must be at least 59 and at most 76.
// hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
// ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
// pid (Passport ID) - a nine-digit number, including leading zeroes.
// cid (Country ID) - ignored, missing or not.
