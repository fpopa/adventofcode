package days

import (
	"bufio"
	"fmt"
	"os"
)

func isTree(r string, x int) bool {
	treeSymbol := '#'
	index := x % len(r)

	return []rune(r)[index] == treeSymbol
}

func getEncounteredTrees(lines []string, right, down int) int {
	treesEncountered := 0
	x := 0
	y := 0

	for {
		if y >= len(lines) {
			return treesEncountered
		}

		if isTree(lines[y], x) {
			treesEncountered++
		}

		x += right
		y += down
	}
}

func multiply(numbers ...int) int {
	if len(numbers) < 0 {
		return 0
	}

	res := 1
	for _, n := range numbers {
		res *= n
	}

	return res
}

// Day3 implements day one challenge
func Day3() error {
	lines, err := readFile("inputs/3.txt")
	if err != nil {
		return err
	}

	fmt.Println("part one")
	fmt.Println(getEncounteredTrees(lines, 3, 1))

	fmt.Println("part two")
	fmt.Println(multiply([]int{
		getEncounteredTrees(lines, 1, 1),
		getEncounteredTrees(lines, 3, 1),
		getEncounteredTrees(lines, 5, 1),
		getEncounteredTrees(lines, 7, 1),
		getEncounteredTrees(lines, 1, 2),
	}...))

	return nil
}

func getInputDay3() ([]string, error) {
	lines := []string{}
	f, err := os.Open("inputs/3.txt")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		l := scanner.Text()

		lines = append(lines, l)
	}

	err = scanner.Err()
	if err != nil {
		return nil, err
	}

	return lines, nil
}

// https://adventofcode.com/2020/day/3
// --- Day 3: Toboggan Trajectory ---
// With the toboggan login problems resolved, you set off toward the airport. While travel by toboggan might be easy, it's certainly not safe: there's very minimal steering and the area is covered in trees. You'll need to see which angles will take you near the fewest trees.
//
// Due to the local geology, trees in this area only grow on exact integer coordinates in a grid. You make a map (your puzzle input) of the open squares (.) and trees (#) you can see. For example:
//
// ..##.......
// #...#...#..
// .#....#..#.
// ..#.#...#.#
// .#...##..#.
// ..#.##.....
// .#.#.#....#
// .#........#
// #.##...#...
// #...##....#
// .#..#...#.#
// These aren't the only trees, though; due to something you read about once involving arboreal genetics and biome stability, the same pattern repeats to the right many times:
//
// ..##.........##.........##.........##.........##.........##.......  --->
// #...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
// .#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
// ..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
// .#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
// ..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....  --->
// .#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
// .#........#.#........#.#........#.#........#.#........#.#........#
// #.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
// #...##....##...##....##...##....##...##....##...##....##...##....#
// .#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
// You start on the open square (.) in the top-left corner and need to reach the bottom (below the bottom-most row on your map).
//
// The toboggan can only follow a few specific slopes (you opted for a cheaper model that prefers rational numbers); start by counting all the trees you would encounter for the slope right 3, down 1:
//
// From your starting position at the top-left, check the position that is right 3 and down 1. Then, check the position that is right 3 and down 1 from there, and so on until you go past the bottom of the map.
//
// The locations you'd check in the above example are marked here with O where there was an open square and X where there was a tree:
//
// ..##.........##.........##.........##.........##.........##.......  --->
// #..O#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
// .#....X..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
// ..#.#...#O#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
// .#...##..#..X...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
// ..#.##.......#.X#.......#.##.......#.##.......#.##.......#.##.....  --->
// .#.#.#....#.#.#.#.O..#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
// .#........#.#........X.#........#.#........#.#........#.#........#
// #.##...#...#.##...#...#.X#...#...#.##...#...#.##...#...#.##...#...
// #...##....##...##....##...#X....##...##....##...##....##...##....#
// .#..#...#.#.#..#...#.#.#..#...X.#.#..#...#.#.#..#...#.#.#..#...#.#  --->
// In this example, traversing the map using this slope would cause you to encounter 7 trees.
//
// Starting at the top-left corner of your map and following a slope of right 3 and down 1, how many trees would you encounter?
