package days

import (
	"fmt"
)

func getFirstEncounter(rows []string, called, i, j, dirI, dirJ int) rune {
	floor := '.'

	// outside bounds
	if i+dirI < 0 ||
		i+dirI >= len(rows) ||
		j+dirJ < 0 ||
		j+dirJ >= len(rows[0]) {
		if called == 0 {
			return floor
		}
		return []rune(rows[i])[j]
	}

	if []rune(rows[i+dirI])[j+dirJ] != floor {
		return []rune(rows[i+dirI])[j+dirJ]
	}

	return getFirstEncounter(rows, called+1, i+dirI, j+dirJ, dirI, dirJ)
}

func getFirstEncounterCounts(rows []string, i, j int) (int, int) {
	m := map[rune]int{
		'L': 0,
		'.': 0,
		'#': 0,
	}

	for x := -1; x <= 1; x++ {
		for y := -1; y <= 1; y++ {
			if x == y && x == 0 {
				continue
			}

			m[getFirstEncounter(rows, 0, i, j, x, y)]++
		}
	}

	return m['L'], m['#']
}

func getAdjacentCounts(rows []string, i, j int) (int, int) {
	m := map[rune]int{
		'L': 0,
		'#': 0,
	}

	// row above
	if i > 0 {
		// top left
		if j > 0 {
			m[[]rune(rows[i-1])[j-1]]++
		}

		// top
		m[[]rune(rows[i-1])[j]]++

		// top right
		if j < len(rows[0])-1 {
			m[[]rune(rows[i-1])[j+1]]++
		}
	}
	// left
	if j > 0 {
		m[[]rune(rows[i])[j-1]]++
	}
	// right
	if j < len(rows[0])-1 {
		m[[]rune(rows[i])[j+1]]++
	}
	// row below
	if i < len(rows)-1 {
		// bottom left
		if j > 0 {
			m[[]rune(rows[i+1])[j-1]]++
		}

		// bottom
		m[[]rune(rows[i+1])[j]]++

		// bottom right
		if j < len(rows[0])-1 {
			m[[]rune(rows[i+1])[j+1]]++
		}
	}

	return m['L'], m['#']
}
func applyRound(rows []string) ([]string, int) {
	empty := 'L'
	occupied := '#'
	newRows := []string{}
	changes := 0

	for i, r := range rows {
		newRow := ""
		for j, s := range r {
			// part two
			_, o := getFirstEncounterCounts(rows, i, j)
			// _, o := getAdjacentCounts(rows, i, j)
			switch true {
			case s == empty && o == 0:
				s = occupied
				changes++
			// part one
			// case s == occupied && o >= 4:
			case s == occupied && o >= 5:
				s = empty
				changes++
			}

			newRow += string(s)
		}
		newRows = append(newRows, newRow)
	}

	return newRows, changes
}

func countOccupied(rows []string) int {
	occupied := 0

	for _, r := range rows {
		for _, c := range r {
			if c != '#' {
				continue
			}

			occupied++
		}
	}

	return occupied
}

// Day11 implements day one challenge
func Day11() error {
	rows, err := readFile("inputs/11.txt")
	if err != nil {
		return err
	}

	changes := 1
	for changes != 0 {
		rows, changes = applyRound(rows)
		// for _, r := range rows {
		// 	fmt.Println(r)
		// }
		// fmt.Println("_______")
	}

	o := countOccupied(rows)
	fmt.Println(o)
	// fmt.Println(getFirstEncounterCounts(rows, 0, 2))
	// fmt.Println(string(getFirstEncounter(rows, 0, 1, 1, -1, 0)))
	return nil
}

// --- Day 11: Seating System ---
// Your plane lands with plenty of time to spare. The final leg of your journey is a ferry that goes directly to the tropical island where you can finally start your vacation. As you reach the waiting area to board the ferry, you realize you're so early, nobody else has even arrived yet!
//
// By modeling the process people use to choose (or abandon) their seat in the waiting area, you're pretty sure you can predict the best place to sit. You make a quick map of the seat layout (your puzzle input).
//
// The seat layout fits neatly on a grid. Each position is either floor (.), an empty seat (L), or an occupied seat (#). For example, the initial seat layout might look like this:
//
// L.LL.LL.LL
// LLLLLLL.LL
// L.L.L..L..
// LLLL.LL.LL
// L.LL.LL.LL
// L.LLLLL.LL
// ..L.L.....
// LLLLLLLLLL
// L.LLLLLL.L
// L.LLLLL.LL
// Now, you just need to model the people who will be arriving shortly. Fortunately, people are entirely predictable and always follow a simple set of rules. All decisions are based on the number of occupied seats adjacent to a given seat (one of the eight positions immediately up, down, left, right, or diagonal from the seat). The following rules are applied to every seat simultaneously:
//
// If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
// If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
// Otherwise, the seat's state does not change.
// Floor (.) never changes; seats don't move, and nobody sits on the floor.
//
// After one round of these rules, every seat in the example layout becomes occupied:
//
// #.##.##.##
// #######.##
// #.#.#..#..
// ####.##.##
// #.##.##.##
// #.#####.##
// ..#.#.....
// ##########
// #.######.#
// #.#####.##
// After a second round, the seats with four or more occupied adjacent seats become empty again:
//
// #.LL.L#.##
// #LLLLLL.L#
// L.L.L..L..
// #LLL.LL.L#
// #.LL.LL.LL
// #.LLLL#.##
// ..L.L.....
// #LLLLLLLL#
// #.LLLLLL.L
// #.#LLLL.##
// This process continues for three more rounds:
//
// #.##.L#.##
// #L###LL.L#
// L.#.#..#..
// #L##.##.L#
// #.##.LL.LL
// #.###L#.##
// ..#.#.....
// #L######L#
// #.LL###L.L
// #.#L###.##
// #.#L.L#.##
// #LLL#LL.L#
// L.L.L..#..
// #LLL.##.L#
// #.LL.LL.LL
// #.LL#L#.##
// ..L.L.....
// #L#LLLL#L#
// #.LLLLLL.L
// #.#L#L#.##
// #.#L.L#.##
// #LLL#LL.L#
// L.#.L..#..
// #L##.##.L#
// #.#L.LL.LL
// #.#L#L#.##
// ..L.L.....
// #L#L##L#L#
// #.LLLLLL.L
// #.#L#L#.##
// At this point, something interesting happens: the chaos stabilizes and further applications of these rules cause no seats to change state! Once people stop moving around, you count 37 occupied seats.
//
// Simulate your seating area by applying the seating rules repeatedly until no seats change state. How many seats end up occupied?
