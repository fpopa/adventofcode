package days

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func getBags(rawColors string) []bag {
	bags := []bag{}

	for _, s := range strings.Split(rawColors, ",") {
		count, _ := strconv.Atoi(strings.Split(strings.TrimSpace(s), " ")[0])

		// assuming color names don't contain digits
		reg, err := regexp.Compile("[0-9]+")
		if err != nil {
			log.Fatal(err)
		}
		s = reg.ReplaceAllString(s, "")

		color := strings.TrimSpace(strings.Split(s, "bag")[0])
		bags = append(bags, bag{
			count: count,
			color: color,
		})
		strings.TrimSpace(color)
	}

	return bags
}

func getAllBags(rawBags []string) map[string][]bag {
	rules := map[string][]bag{}

	for _, r := range rawBags {
		// light red bags contain 1 bright white bag, 2 muted yellow bags.
		parts := strings.Split(r, "contain")
		color := strings.Split(parts[0], "bag")[0]
		rules[strings.TrimSpace(color)] = getBags(parts[1])
	}

	return rules
}

func getAncestors(descendant string, tree map[string][]bag) map[string]bool {
	ancestors := map[string]bool{}

	for parent, children := range tree {
		for _, c := range children {
			if c.color != descendant {
				continue
			}

			ancestors[parent] = true
			for k := range getAncestors(parent, tree) {
				ancestors[k] = true
			}
		}
	}

	return ancestors
}

func getDescendantsCount(parent string, tree map[string][]bag) int {
	descendants := 0

	bags, ok := tree[parent]
	if !ok {
		return descendants
	}

	for _, bag := range bags {
		descendants += bag.count + bag.count*getDescendantsCount(bag.color, tree)
	}

	return descendants
}

type bag struct {
	count int
	color string
}

// Day7 implements day one challenge
func Day7() error {
	lines, err := readFile("inputs/7.txt")
	if err != nil {
		return err
	}

	bags := getAllBags(lines)
	fmt.Println(len(getAncestors("shiny gold", bags)))
	fmt.Println(getDescendantsCount("shiny gold", bags))
	return nil
}

// --- Day 7: Handy Haversacks ---
// You land at the regional airport in time for your next flight. In fact, it looks like you'll even have time to grab some food: all flights are currently delayed due to issues in luggage processing.
//
// Due to recent aviation regulations, many rules (your puzzle input) are being enforced about bags and their contents; bags must be color-coded and must contain specific quantities of other color-coded bags. Apparently, nobody responsible for these regulations considered how long they would take to enforce!
//
// For example, consider the following rules:
//
// light red bags contain 1 bright white bag, 2 muted yellow bags.
// dark orange bags contain 3 bright white bags, 4 muted yellow bags.
// bright white bags contain 1 shiny gold bag.
// muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
// shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
// dark olive bags contain 3 faded blue bags, 4 dotted black bags.
// vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
// faded blue bags contain no other bags.
// dotted black bags contain no other bags.
// These rules specify the required contents for 9 bag types. In this example, every faded blue bag is empty, every vibrant plum bag contains 11 bags (5 faded blue and 6 dotted black), and so on.
//
// You have a shiny gold bag. If you wanted to carry it in at least one other bag, how many different bag colors would be valid for the outermost bag? (In other words: how many colors can, eventually, contain at least one shiny gold bag?)
//
// In the above rules, the following options would be available to you:
//
// A bright white bag, which can hold your shiny gold bag directly.
// A muted yellow bag, which can hold your shiny gold bag directly, plus some other bags.
// A dark orange bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.
// A light red bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.
// So, in this example, the number of bag colors that can eventually contain at least one shiny gold bag is 4.
//
// How many bag colors can eventually contain at least one shiny gold bag? (The list of rules is quite long; make sure you get all of it.)
