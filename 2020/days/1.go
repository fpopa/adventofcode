package days

import (
	"fmt"

	"gitlab.com/fpopa/AdventOfCode/2020/go/inputs"
)

// findSumOf returns an array of length of l of numbers that sum up to the passed sum parameter
//
// sum - expected sum
// non - number of numbers
func findSumOf(sum int, non int, inputNumbers []int, retNumbers []int) ([]int, bool) {
	//fmt.Printf("sum %v l %v numbers: %#v retNumbers:%#v\n", sum, l, numbers, retNumbers)
	for i := 0; i < len(inputNumbers); i++ {
		// if we're only searching for last number we can check value against remaining sum
		if non == 1 {
			if sum != inputNumbers[i] {
				continue
			}

			return append(retNumbers, inputNumbers[i]), true
		}

		recursiveResult, ok := findSumOf(
			sum-inputNumbers[i],
			non-1,
			inputNumbers[i+1:],
			append(retNumbers, inputNumbers[i]),
		)
		if !ok {
			continue
		}

		return recursiveResult, true
	}

	return nil, false
}

// Day1 implements day one challenge
func Day1() error {
	input := inputs.Day1()

	// part1
	//numberOfNumbers := 2
	// part2
	numberOfNumbers := 3
	numbers, ok := findSumOf(
		2020,
		numberOfNumbers,
		input,
		[]int{},
	)

	fmt.Printf("numbers: %#v %v\n", numbers, ok)
	return nil
}

// https://adventofcode.com/2020/day/1
// --- Day 1: Report Repair ---
// After saving Christmas five years in a row, you've decided to take a vacation at a nice resort on a tropical island. Surely, Christmas will go on without you.
//
// The tropical island has its own currency and is entirely cash-only. The gold coins used there have a little picture of a starfish; the locals just call them stars. None of the currency exchanges seem to have heard of them, but somehow, you'll need to find fifty of these coins by the time you arrive so you can pay the deposit on your room.
//
// To save your vacation, you need to get all fifty stars by December 25th.
//
// Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!
//
// Before you leave, the Elves in accounting just need you to fix your expense report (your puzzle input); apparently, something isn't quite adding up.
//
// Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.
//
// For example, suppose your expense report contained the following:
//
// 1721
// 979
// 366
// 299
// 675
// 1456
// In this list, the two entries that sum to 2020 are 1721 and 299. Multiplying them together produces 1721 * 299 = 514579, so the correct answer is 514579.
//
// Of course, your expense report is much larger. Find the two entries that sum to 2020; what do you get if you multiply them together?
