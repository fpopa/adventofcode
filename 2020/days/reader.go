package days

import (
	"bufio"
	"os"
)

func readFile(path string) ([]string, error) {
	lines := []string{}
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		l := scanner.Text()

		lines = append(lines, l)
	}

	err = scanner.Err()
	if err != nil {
		return nil, err
	}

	return lines, nil
}
