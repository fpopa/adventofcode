package days

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strings"
)

func mostCommonMinusLeastCommon(counts map[rune]int) int {
	min := math.MaxInt64
	max := 0
	for _, v := range counts {
		if v < min {
			min = v
			continue
		}

		if v > max {
			max = v
		}
	}

	return max - min
}

func getPolimerCountsAfterSteps(template []rune, pairInsertions map[string]rune, steps int) map[rune]int {
	polymer := template
	for steps > 0 {
		newPolymer := []rune{}

		for i := 1; i < len(polymer); i++ {
			v := pairInsertions[fmt.Sprintf("%c%c", polymer[i-1], polymer[i])]
			newPolymer = append(newPolymer, polymer[i-1], v)
		}
		// add last element of the previous polymer
		newPolymer = append(newPolymer, polymer[len(polymer)-1])

		polymer = newPolymer
		steps--
	}

	counts := map[rune]int{}
	for _, c := range polymer {
		if _, ok := counts[c]; !ok {
			counts[c] = 0
		}

		counts[c]++
	}

	return counts
}

// getPolymerPairCounts returns a map for the provided polymer:
//
// for NNCB polymer the result would look like this:
// NN -> 1
// NC -> 1
// CB -> 1
func getPolymerPairCounts(polymer []rune) map[string]int {
	pairCounts := map[string]int{}
	for i := 1; i < len(polymer); i++ {
		key := fmt.Sprintf("%c%c", polymer[i-1], polymer[i])

		if _, ok := pairCounts[key]; !ok {
			pairCounts[key] = 0
		}

		pairCounts[key]++
	}

	return pairCounts
}

func getInitialElementCounts(polymer []rune) map[rune]int {
	elementCounts := map[rune]int{}

	for _, charIndex := range polymer {
		if _, ok := elementCounts[charIndex]; !ok {
			elementCounts[charIndex] = 0
		}

		elementCounts[charIndex]++
	}

	return elementCounts
}

func getPolimerCountsv2AfterSteps(template []rune, pairInsertions map[string]rune, steps int) map[rune]int {
	polymerPairCounts := getPolymerPairCounts(template)
	elementCounts := getInitialElementCounts(template)
	// fmt.Println("polimercounts")
	// for k, v := range polymerPairCounts {
	// 	fmt.Printf("%s -> %d\n", k, v)
	// }
	// fmt.Println("elementsCounts")
	// for k, v := range elementCounts {
	// 	fmt.Printf("%c -> %d\n", k, v)
	// }
	// fmt.Println("-------")

	for steps > 0 {
		newPolymerPairCounts := map[string]int{}
		for pair, counts := range polymerPairCounts {
			insertedElement := pairInsertions[pair]
			elementCounts[insertedElement] += counts

			leftPair := fmt.Sprintf("%c%c", pair[0], insertedElement)
			rightPair := fmt.Sprintf("%c%c", insertedElement, pair[1])

			if _, ok := newPolymerPairCounts[leftPair]; !ok {
				newPolymerPairCounts[leftPair] = 0
			}
			if _, ok := newPolymerPairCounts[rightPair]; !ok {
				newPolymerPairCounts[rightPair] = 0
			}
			newPolymerPairCounts[leftPair] += counts
			newPolymerPairCounts[rightPair] += counts
			// fmt.Println(pair, "->", leftPair, " ", rightPair)
		}

		polymerPairCounts = newPolymerPairCounts
		// fmt.Println("polimercounts")
		// for k, v := range polymerPairCounts {
		// 	fmt.Printf("%s -> %d\n", k, v)
		// }
		// fmt.Println("elementsCounts")
		// for k, v := range elementCounts {
		// 	fmt.Printf("%c -> %d\n", k, v)
		// }
		// fmt.Println("-------")
		steps--
	}

	return elementCounts
}

// Day14 implements day two challenge
func Day14() error {
	template, pairInsertions := getInputDay14()

	part1Output := mostCommonMinusLeastCommon(getPolimerCountsAfterSteps(template, pairInsertions, 10))
	fmt.Println(part1Output)

	part2Output := mostCommonMinusLeastCommon(getPolimerCountsv2AfterSteps(template, pairInsertions, 40))
	fmt.Println(part2Output)

	return nil
}

func getInputDay14() ([]rune, map[string]rune) {
	f, err := os.Open("inputs/14.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	var template []rune
	pairInsertions := map[string]rune{}
	foundEmptyLine := false
	for scanner.Scan() {
		stringRow := scanner.Text()

		if stringRow == "" {
			foundEmptyLine = true
			continue
		}

		if !foundEmptyLine {
			template = []rune(stringRow)
			continue
		}

		parts := strings.Split(stringRow, " -> ")
		pairInsertions[parts[0]] = []rune(parts[1])[0]
	}

	err = scanner.Err()
	if err != nil {
		panic(err)
	}

	return template, pairInsertions
}
