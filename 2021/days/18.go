package days

import (
	"fmt"
	"strconv"
)

type snailfishElement struct {
	value int
	depth int
}

func parseSnailFishPairs(stringSnailfish string) []snailfishElement {
	rawSnailfish := []rune(stringSnailfish)
	foundSnailfish := []snailfishElement{}
	depth := 0
	for i := 0; i < len(rawSnailfish); i++ {
		switch rawSnailfish[i] {
		case '[':
			depth++
			continue
		case ']':
			depth--
			continue
		case ',':
			continue
		default:
			value, err := strconv.Atoi(string(rawSnailfish[i]))
			panicOnError(err)
			foundSnailfish = append(foundSnailfish, snailfishElement{
				value: value,
				depth: depth,
			})
		}
	}
	return foundSnailfish
}

func getFinalSnailfish(rawSnailFish []string) []snailfishElement {
	currentSnailfish := parseSnailFishPairs(rawSnailFish[0])
	rawSnailFish = rawSnailFish[1:]

	reductionHappened := true
	for reductionHappened {
		reductionHappened = false

		for i := range currentSnailfish {
			if currentSnailfish[i].depth == 5 {
				if i > 0 {
					currentSnailfish[i-1].value += currentSnailfish[i].value
				}
				if i+2 < len(currentSnailfish) {
					currentSnailfish[i+2].value += currentSnailfish[i+1].value
				}

				currentSnailfish[i].value = 0
				currentSnailfish[i].depth--
				currentSnailfish = append(currentSnailfish[:i+1], currentSnailfish[i+2:]...)
				reductionHappened = true
				break
			}
		}
		if reductionHappened {
			continue
		}

		for i := range currentSnailfish {
			if currentSnailfish[i].value >= 10 {
				currentSnailfish[i].depth++

				newSnailfish := snailfishElement{
					depth: currentSnailfish[i].depth,
					value: currentSnailfish[i].value/2 + currentSnailfish[i].value%2,
				}

				currentSnailfish[i].value /= 2

				appendSnailfish := append([]snailfishElement{}, currentSnailfish[i+1:]...)
				currentSnailfish = append(currentSnailfish[:i+1], newSnailfish)
				currentSnailfish = append(currentSnailfish, appendSnailfish...)
				reductionHappened = true
				break
			}
		}
		if reductionHappened {
			continue
		}

		if len(rawSnailFish) > 0 {
			reductionHappened = true
			currentSnailfish = append(currentSnailfish, parseSnailFishPairs(rawSnailFish[0])...)
			rawSnailFish = rawSnailFish[1:]

			for i := range currentSnailfish {
				currentSnailfish[i].depth++
			}
		}
	}

	return currentSnailfish
}

func getFinalSum(rawSnailFish []string) int {
	finalSnailfish := getFinalSnailfish(rawSnailFish)

	for depth := 4; depth >= 0; {
		found := false
		if len(finalSnailfish) == 1 {
			break
		}
		for i := range finalSnailfish {
			if finalSnailfish[i].depth == depth {
				finalSnailfish[i].depth--
				finalSnailfish[i].value = 3*finalSnailfish[i].value + 2*finalSnailfish[i+1].value
				found = true
				newFinalSnailfish := append([]snailfishElement{}, finalSnailfish[:i+1]...)
				if len(finalSnailfish) > i+2 {
					newFinalSnailfish = append(newFinalSnailfish, finalSnailfish[i+2:]...)
				}
				finalSnailfish = newFinalSnailfish
				break
			}
		}

		if !found {
			depth--
		}
	}

	return finalSnailfish[0].value
}

func getHighestMagnitudeOfTwo(rawSnailFish []string) int {
	maxMagnitude := 0
	for i := 0; i < len(rawSnailFish)-1; i++ {
		for j := i + 1; j < len(rawSnailFish); j++ {
			magnitude := getFinalSum([]string{rawSnailFish[i], rawSnailFish[j]})
			if magnitude > maxMagnitude {
				maxMagnitude = magnitude
			}
			magnitude = getFinalSum([]string{rawSnailFish[j], rawSnailFish[i]})
			if magnitude > maxMagnitude {
				maxMagnitude = magnitude
			}
		}
	}

	return maxMagnitude
}

// Day18 implements day two challenge
func Day18() error {
	rawSnailfish := getInputDay18()
	fmt.Println(getFinalSum(rawSnailfish))
	fmt.Println(getHighestMagnitudeOfTwo(rawSnailfish))

	return nil
}

func getInputDay18() []string {
	ret := []string{
		"[[2,[2,3]],[[[0,0],[2,2]],[[3,3],[3,5]]]]",
		"[[[8,[8,8]],6],[9,5]]",
		"[[[[5,2],3],[[5,8],[1,1]]],[[[4,2],3],[[1,6],4]]]",
		"[[[[6,8],[0,9]],8],[[[9,4],6],[8,6]]]",
		"[9,[[6,7],4]]",
		"[[1,[3,6]],[5,[[7,4],6]]]",
		"[[[[4,7],6],[[8,9],5]],[[[6,2],[2,7]],[[9,0],[7,0]]]]",
		"[[[[7,3],4],[7,[7,4]]],1]",
		"[4,[6,6]]",
		"[[3,[3,2]],[[7,1],[[6,4],[6,1]]]]",
		"[[[[8,7],4],8],[[[8,9],5],[6,[2,7]]]]",
		"[[4,[3,[4,1]]],8]",
		"[5,[[5,1],9]]",
		"[[3,[[2,4],[3,5]]],[3,[8,6]]]",
		"[[[1,9],[[4,0],[8,5]]],[[0,[1,0]],[5,[8,7]]]]",
		"[[[6,6],[[5,0],[3,4]]],[[3,7],6]]",
		"[[[[0,7],[6,3]],[[2,6],8]],[[[3,0],8],[[4,0],[6,8]]]]",
		"[[[[0,4],[6,3]],[1,[9,1]]],[[1,[1,4]],9]]",
		"[[[[8,3],2],[0,[6,8]]],[5,[[4,4],[1,8]]]]",
		"[[[[1,0],[7,8]],6],[3,[[5,4],[4,2]]]]",
		"[2,[9,5]]",
		"[[4,[2,[0,0]]],[[1,3],[1,9]]]",
		"[6,[[[2,6],2],9]]",
		"[[6,[1,[7,9]]],[[[7,6],[8,8]],[1,7]]]",
		"[[[3,7],[6,9]],[5,2]]",
		"[[[6,1],9],[[9,7],[2,[9,1]]]]",
		"[[[[2,9],7],[[8,1],[2,1]]],[4,[[3,0],9]]]",
		"[[[0,0],[[8,9],[2,8]]],[[[8,4],5],[0,[1,0]]]]",
		"[[[[6,5],[3,6]],[[6,0],[0,4]]],[[[4,1],[4,2]],[5,1]]]",
		"[[[6,[2,9]],[0,7]],[8,[[7,7],[9,9]]]]",
		"[3,[[7,[5,7]],[6,[9,7]]]]",
		"[[0,[3,[9,9]]],[[3,[5,8]],[3,[6,5]]]]",
		"[[[5,[7,1]],[[9,9],[7,0]]],[0,8]]",
		"[[[1,[4,5]],[5,[4,6]]],[1,[[1,0],9]]]",
		"[[[[4,2],7],[[0,6],7]],[8,[[6,8],0]]]",
		"[9,[3,[[7,3],9]]]",
		"[[7,6],[[1,[2,8]],[[3,2],[9,1]]]]",
		"[[[0,5],[3,[6,6]]],[[[1,5],[1,8]],[[8,9],8]]]",
		"[[8,[[1,6],[2,0]]],[[[3,7],3],0]]",
		"[[[0,[6,2]],[9,[5,8]]],[[[1,1],4],[8,4]]]",
		"[[[[3,5],[5,8]],7],[[3,6],8]]",
		"[[5,[1,0]],[[[1,3],6],[[6,8],5]]]",
		"[[[[7,4],0],[5,1]],[[8,7],4]]",
		"[[3,[8,3]],[[8,[3,8]],6]]",
		"[[1,[[0,7],[2,7]]],[[2,9],[6,[8,3]]]]",
		"[[[5,[1,9]],[2,[7,0]]],[[[5,3],2],[1,[9,1]]]]",
		"[[[[0,0],[0,9]],[[0,8],4]],[[7,[3,9]],4]]",
		"[[4,0],[0,4]]",
		"[1,[[[5,5],4],[[7,7],3]]]",
		"[[[[0,0],[9,9]],[[9,8],[8,1]]],[[[1,4],[0,2]],[1,0]]]",
		"[[[1,[4,0]],1],[4,[6,5]]]",
		"[2,[[[3,3],4],[[2,9],[3,9]]]]",
		"[[[[3,2],[2,6]],[[5,8],[1,1]]],[[[4,9],9],1]]",
		"[[[5,[1,1]],2],[[[2,9],3],[3,4]]]",
		"[[[0,[6,2]],[4,[3,8]]],[[[3,5],[6,5]],[[9,9],2]]]",
		"[[[[1,2],5],[[5,2],[3,0]]],[[6,[0,1]],[[3,5],8]]]",
		"[[2,[[5,2],[5,5]]],[3,[[1,1],2]]]",
		"[[[[4,1],[8,8]],[[2,5],2]],[[[1,4],[3,3]],1]]",
		"[[[1,1],[2,[3,6]]],[[[0,8],[3,1]],[[1,1],[2,6]]]]",
		"[[0,[[5,1],5]],[1,[[0,0],7]]]",
		"[[[4,1],[[2,7],4]],[[[8,1],[2,2]],[[3,1],[1,7]]]]",
		"[[[1,[7,4]],[[1,8],[7,4]]],[[2,3],[7,[9,6]]]]",
		"[[[9,6],[[6,1],5]],[[[9,2],3],[[2,4],8]]]",
		"[[[[8,8],2],9],[5,0]]",
		"[[[8,5],[2,1]],[8,[2,9]]]",
		"[[[[5,3],9],2],[[[1,0],[2,4]],5]]",
		"[[[[0,8],0],1],[[5,[4,1]],[5,2]]]",
		"[[[[6,4],[6,2]],[3,[4,0]]],[[[9,6],8],[[6,8],[5,3]]]]",
		"[[9,9],[1,1]]",
		"[[0,[[9,2],1]],[[[6,4],[8,3]],6]]",
		"[[[8,9],[4,3]],[[2,[2,7]],[[2,3],8]]]",
		"[[[[8,4],[7,5]],[4,2]],[[[9,4],0],[[9,2],[7,9]]]]",
		"[8,[[[6,8],3],[[5,3],5]]]",
		"[[[[9,3],0],[1,4]],[[7,[4,7]],4]]",
		"[[[5,[4,6]],6],[[[8,0],1],[[1,8],0]]]",
		"[[[[0,9],[1,7]],[3,9]],[[[2,7],[5,2]],[[4,6],[7,0]]]]",
		"[[[5,[0,5]],5],[[[8,9],2],[9,6]]]",
		"[[6,[6,[9,0]]],[[[7,3],[5,0]],[2,[1,5]]]]",
		"[2,[[9,6],[3,[3,7]]]]",
		"[[[1,6],[7,1]],9]",
		"[[[[2,4],2],[[6,1],[6,3]]],[[6,[9,7]],6]]",
		"[[[[6,6],[2,9]],[[9,6],[3,5]]],[5,3]]",
		"[[[[7,2],6],6],[3,[2,[8,2]]]]",
		"[[[[6,9],[6,9]],3],[[[6,5],4],8]]",
		"[7,[[1,8],[[2,1],5]]]",
		"[[9,5],9]",
		"[[[8,9],[6,4]],[[2,2],[[3,5],[9,0]]]]",
		"[[[[2,3],8],[1,8]],[[[8,2],[3,8]],[[0,3],2]]]",
		"[[0,[2,[1,9]]],[9,0]]",
		"[[[[7,9],[0,8]],7],[5,[[3,8],[0,4]]]]",
		"[[[8,9],[1,[6,0]]],[[5,3],4]]",
		"[7,[[9,9],7]]",
		"[[[[6,8],2],[[4,4],[4,6]]],[[1,[4,6]],[2,7]]]",
		"[[6,2],[5,[2,1]]]",
		"[[[6,0],[[0,9],[8,3]]],[7,[[1,1],[0,1]]]]",
		"[[[0,[0,6]],[8,0]],0]",
		"[[3,[[4,8],5]],[[7,3],[5,0]]]",
		"[[6,[8,[0,2]]],2]",
		"[[[7,2],6],3]",
		"[[[3,[1,1]],3],[[7,9],[2,[2,3]]]]",
	}
	return ret
}
