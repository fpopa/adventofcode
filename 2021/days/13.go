package days

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func foldMatrix(mat [][]int, folds []fold) [][]int {
	for _, f := range folds {
		if f.axis == "y" {
			for i := 0; i < f.index; i++ {
				for j := 0; j < len(mat[0]); j++ {
					mat[i][j] += mat[len(mat)-i-1][j]
				}
			}
			mat = mat[:f.index]
		}
		if f.axis == "x" {
			for i := 0; i < len(mat); i++ {
				for j := 0; j < f.index; j++ {
					mat[i][j] += mat[i][len(mat[0])-j-1]
				}
			}

			for i := range mat {
				mat[i] = mat[i][:f.index]
			}
		}
	}

	return mat
}

func countElements(mat [][]int) int {
	count := 0
	for i := range mat {
		for j := range mat[0] {
			if mat[i][j] == 0 {
				continue
			}
			count++
		}
	}

	return count
}

// Day13 implements day two challenge
func Day13() error {
	matrix, folds := getInputDay13()

	part1Output := countElements(foldMatrix(matrix, folds[:1]))
	fmt.Println(part1Output)

	part2Output := foldMatrix(matrix, folds)
	printMatrix(part2Output)

	return nil
}

type fold struct {
	axis  string
	index int
}

func getInputDay13() ([][]int, []fold) {
	f, err := os.Open("inputs/13.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	folds := []fold{}
	foundEmptyLine := false
	maxX, maxY := 0, 0
	matrixElements := [][]int{}
	for scanner.Scan() {
		stringRow := scanner.Text()

		if stringRow == "" {
			foundEmptyLine = true
			continue
		}

		if !foundEmptyLine {
			parts := strings.Split(stringRow, ",")
			x, err := strconv.Atoi(parts[0])
			if err != nil {
				panic(err)
			}
			if x > maxX {
				maxX = x
			}

			y, err := strconv.Atoi(parts[1])
			if err != nil {
				panic(err)
			}
			if y > maxY {
				maxY = y
			}

			matrixElements = append(matrixElements, []int{x, y})
			continue
		}

		axisIndex := strings.Split(stringRow, "fold along ")[1]
		parts := strings.Split(axisIndex, "=")

		index, err := strconv.Atoi(parts[1])
		if err != nil {
			panic(err)
		}

		folds = append(folds, fold{
			axis:  parts[0],
			index: index,
		})
	}

	var matrix = make([][]int, maxY+1)
	for i := range matrix {
		matrix[i] = make([]int, maxX+1)
	}

	for _, e := range matrixElements {
		matrix[e[1]][e[0]] = 1
	}

	err = scanner.Err()
	if err != nil {
		panic(err)
	}

	return matrix, folds
}
