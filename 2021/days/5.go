package days

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const (
	withDiagonals    = true
	withoutDiagonals = false
)

func overlappingPoints(lines []line, withDiagonals bool) int {
	overlappingPointsMap := map[string]int{}
	for _, l := range lines {
		if l.a.x != l.b.x && l.a.y != l.b.y && !withDiagonals {
			continue
		}
		for _, p := range l.getLinePoints() {
			if _, ok := overlappingPointsMap[p.String()]; !ok {
				overlappingPointsMap[p.String()] = 0
			}

			overlappingPointsMap[p.String()]++
		}
	}

	overlappingPointsCount := 0
	for _, v := range overlappingPointsMap {
		if v >= 2 {
			overlappingPointsCount++
		}
	}
	return overlappingPointsCount
}

// Day5 implements day two challenge
func Day5() error {
	lines, err := getInputDay5()
	if err != nil {
		return err
	}

	part1Output := overlappingPoints(lines, withoutDiagonals)
	fmt.Printf("part1: %v\n", part1Output)

	part2Output := overlappingPoints(lines, withDiagonals)
	fmt.Printf("part2: %v\n", part2Output)

	return nil
}

type point struct {
	x, y int
}

func (p point) String() string {
	return fmt.Sprintf("%d-%d", p.x, p.y)
}

type line struct {
	a point
	b point
}

func getDirectionFromAToB(a point, b point) (int, int) {
	switch true {
	case a.x == b.x:
		if a.y < b.y {
			return 0, 1
		}
		return 0, -1

	case a.y == b.y:
		if a.x < b.x {
			return 1, 0
		}
		return -1, 0
	case a.y < b.y && a.x < b.x:
		return 1, 1
	case a.y > b.y && a.x > b.x:
		return -1, -1
	case a.y < b.y && a.x > b.x:
		return -1, 1
	case a.y > b.y && a.x < b.x:
		return 1, -1
	}

	return 0, 0
}

func (l line) getLinePoints() []point {
	points := []point{}
	directionX, directionY := getDirectionFromAToB(l.a, l.b)

	pointAX := l.a.x
	pointAY := l.a.y
	for pointAX != l.b.x || pointAY != l.b.y {
		points = append(points, point{
			x: pointAX,
			y: pointAY,
		})

		pointAX += directionX
		pointAY += directionY
	}

	return append(points, point{
		x: l.b.x,
		y: l.b.y,
	})
}

func getInputDay5() ([]line, error) {
	getPoint := func(s string) point {
		parts := strings.Split(s, ",")
		x, _ := strconv.Atoi(parts[0])
		y, _ := strconv.Atoi(parts[1])

		return point{
			x: x,
			y: y,
		}
	}

	f, err := os.Open("inputs/5.txt")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	lines := []line{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		row := scanner.Text()

		l := line{}

		parts := strings.Split(row, " -> ")
		l.a = getPoint(parts[0])
		l.b = getPoint(parts[1])
		lines = append(lines, l)
	}

	err = scanner.Err()
	if err != nil {
		return nil, err
	}

	return lines, nil
}

// https://adventofcode.com/2021/day/5
/*
--- Day 5: Hydrothermal Venture ---
You come across a field of hydrothermal vents on the ocean floor! These vents constantly produce large, opaque clouds, so it would be best to avoid them if possible.

They tend to form in lines; the submarine helpfully produces a list of nearby lines of vents (your puzzle input) for you to review. For example:

0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
Each line of vents is given as a line segment in the format x1,y1 -> x2,y2 where x1,y1 are the coordinates of one end the line segment and x2,y2 are the coordinates of the other end. These line segments include the points at both ends. In other words:

An entry like 1,1 -> 1,3 covers points 1,1, 1,2, and 1,3.
An entry like 9,7 -> 7,7 covers points 9,7, 8,7, and 7,7.
For now, only consider horizontal and vertical lines: lines where either x1 = x2 or y1 = y2.

So, the horizontal and vertical lines from the above list would produce the following diagram:

.......1..
..1....1..
..1....1..
.......1..
.112111211
..........
..........
..........
..........
222111....
In this diagram, the top left corner is 0,0 and the bottom right corner is 9,9. Each position is shown as the number of lines which cover that point or . if no line covers that point. The top-left pair of 1s, for example, comes from 2,2 -> 2,1; the very bottom row is formed by the overlapping lines 0,9 -> 5,9 and 0,9 -> 2,9.

To avoid the most dangerous areas, you need to determine the number of points where at least two lines overlap. In the above example, this is anywhere in the diagram with a 2 or larger - a total of 5 points.

Consider only horizontal and vertical lines. At how many points do at least two lines overlap?

--- Part Two ---
Unfortunately, considering only horizontal and vertical lines doesn't give you the full picture; you need to also consider diagonal lines.

Because of the limits of the hydrothermal vent mapping system, the lines in your list will only ever be horizontal, vertical, or a diagonal line at exactly 45 degrees. In other words:

An entry like 1,1 -> 3,3 covers points 1,1, 2,2, and 3,3.
An entry like 9,7 -> 7,9 covers points 9,7, 8,8, and 7,9.
Considering all lines from the above example would now produce the following diagram:

1.1....11.
.111...2..
..2.1.111.
...1.2.2..
.112313211
...1.2....
..1...1...
.1.....1..
1.......1.
222111....
You still need to determine the number of points where at least two lines overlap. In the above example, this is still anywhere in the diagram with a 2 or larger - now a total of 12 points.

Consider all of the lines. At how many points do at least two lines overlap?


*/
