// inspired from https://github.com/joeyemerson/aoc/blob/main/2021/19-beacon-scanner/solution.js
package days

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func rotate(orientation, x, y, z int) (int, int, int) {
	direction := orientation / 4
	rotation := orientation % 4

	if direction == 1 {
		y, z = z, -y
	}
	// go down
	if direction == 2 {
		y, z = -z, y
	}
	// go left
	if direction == 3 {
		x, z = -z, x
	}
	// go right
	if direction == 4 {
		x, z = z, -x
	}
	// look behind
	if direction == 5 {
		x, z = -x, -z
	}

	// 90deg right
	if rotation == 1 {
		x, y = -y, x
	}
	// 180deg right
	if rotation == 2 {
		x, y = -x, -y
	}
	// 270deg right
	if rotation == 3 {
		x, y = y, -x
	}
	return x, y, z
}

func getP2ScannerFromP1(p1pos, p2pos [][]int) *scanner {
	for rotation := 0; rotation <= 24; rotation++ {
		rotatedP2pos := [][]int{}
		for _, v := range p2pos {
			v0, v1, v2 := rotate(rotation, v[0], v[1], v[2])
			rotatedP2pos = append(rotatedP2pos, []int{v0, v1, v2})
		}

		correct := false
		commonDiffCounts := map[string]int{}
		x, y, z := 0, 0, 0
		for i := 0; i < len(p1pos); i++ {
			if correct {
				break
			}
			for j := 0; j < len(rotatedP2pos); j++ {
				x = p1pos[i][0] - rotatedP2pos[j][0]
				y = p1pos[i][1] - rotatedP2pos[j][1]
				z = p1pos[i][2] - rotatedP2pos[j][2]
				key := fmt.Sprintf("%d,%d,%d", x, y, z)
				if _, ok := commonDiffCounts[key]; !ok {
					commonDiffCounts[key] = 0
				}

				commonDiffCounts[key]++
				if commonDiffCounts[key] >= 12 {
					fmt.Println(key)
					fmt.Println(p1pos[i])
					fmt.Println(rotatedP2pos[j])
					correct = true
					break
				}
			}
		}

		if !correct {
			continue
		}

		return &scanner{
			x:        x,
			y:        y,
			z:        z,
			rotation: rotation,
		}
	}

	return nil
}

type scanner struct {
	x, y, z  int
	rotation int
}

func getNumberOfBeacons(scannerReads map[int][][]int) (int, int) {
	scanners := map[int]*scanner{
		0: {
			x:        0,
			y:        0,
			z:        0,
			rotation: 0,
		},
	}

	for len(scanners) < len(scannerReads) {
		for searchingScannerI := 0; searchingScannerI < len(scannerReads); searchingScannerI++ {
			if _, ok := scanners[searchingScannerI]; ok {
				continue
			}

			for knownScannerI := range scanners {
				if searchingScannerI == knownScannerI {
					continue
				}

				s := getP2ScannerFromP1(scannerReads[knownScannerI], scannerReads[searchingScannerI])
				if s == nil {
					continue
				}

				newScannerReads := [][]int{}
				for _, v := range scannerReads[searchingScannerI] {
					x, y, z := rotate(s.rotation, v[0], v[1], v[2])
					newScannerReads = append(newScannerReads, []int{x, y, z})
				}
				scannerReads[searchingScannerI] = newScannerReads
				scanners[searchingScannerI] = s
				s.x += scanners[knownScannerI].x
				s.y += scanners[knownScannerI].y
				s.z += scanners[knownScannerI].z

				fmt.Printf("Found scanner %d using scanner %d\n", searchingScannerI, knownScannerI)
				fmt.Printf("Scanner pos %d,%d,%d\n", s.x, s.y, s.z)
				fmt.Println()

				break
			}
		}
	}

	distinctPoints := map[string]bool{}
	for scannerIndex, v := range scannerReads {
		for _, p := range v {
			x, y, z := p[0], p[1], p[2]
			x += scanners[scannerIndex].x
			y += scanners[scannerIndex].y
			z += scanners[scannerIndex].z

			distinctPoints[fmt.Sprintf("%d,%d,%d", x, y, z)] = true
		}
	}

	maxDistance := 0
	for i := 0; i < len(scanners)-1; i++ {
		for j := i + 1; j < len(scanners); j++ {
			d := int(math.Abs(float64(scanners[i].x - scanners[j].x)))
			d += int(math.Abs(float64(scanners[i].y - scanners[j].y)))
			d += int(math.Abs(float64(scanners[i].z - scanners[j].z)))
			if d > maxDistance {
				maxDistance = d
			}
		}
	}
	return len(distinctPoints), maxDistance
}

// Day19 implements day two challenge
func Day19() error {
	scannerReads := getInputDay19()

	fmt.Println(getNumberOfBeacons(scannerReads))

	return nil
}

// returns map key -> scanner, []int tuples of x y z scan values
func getInputDay19() map[int][][]int {
	currentScanner := -1
	f, err := os.Open("inputs/19.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	scans := map[int][][]int{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		stringRow := scanner.Text()
		if strings.Contains(stringRow, "scanner") {
			currentScanner++
			continue
		}
		if stringRow == "" {
			continue
		}

		parts := strings.Split(stringRow, ",")
		readings := []int{}
		for _, stringValue := range parts {
			v, err := strconv.Atoi(stringValue)
			if err != nil {
				panic(err)
			}

			readings = append(readings, v)
		}

		scans[currentScanner] = append(scans[currentScanner], readings)
	}

	err = scanner.Err()
	if err != nil {
		panic(err)
	}

	return scans
}
