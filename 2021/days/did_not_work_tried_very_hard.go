package days

// import (
// 	"bufio"
// 	"fmt"
// 	"math"
// 	"os"
// 	"strconv"
// 	"strings"
// )

// func getReadingDiffs(readings [][]int) map[string][][]int {
// 	readingdiffs := map[string][][]int{}

// 	for i := 0; i < len(readings)-1; i++ {
// 		for j := i + 1; j < len(readings); j++ {
// 			r1 := readings[i]
// 			r2 := readings[j]

// 			key := fmt.Sprintf("%d,%d,%d",
// 				int(math.Abs(float64(r1[0])-float64(r2[0]))),
// 				int(math.Abs(float64(r1[1])-float64(r2[1]))),
// 				int(math.Abs(float64(r1[2])-float64(r2[2]))),
// 			)
// 			readingdiffs[key] = [][]int{r1, r2}
// 		}
// 	}

// 	return readingdiffs
// }

// var debug = false

// func getCommonDiffs(dif1, dif2 map[string][][]int) ([][]int, [][]int) {
// 	// fmt.Println(len(dif1), len(dif2))
// 	p1Found := map[string]bool{}

// 	commonPointP1 := [][]int{}
// 	commonPointP2 := [][]int{}
// 	for k, v1 := range dif1 {
// 		v2, ok := dif2[k]
// 		if !ok {
// 			continue
// 		}

// 		// we could avoid duplicating points here for now but I'll move along.
// 		// should work with any index: l = 0,1,2
// 		l := 0
// 		if v1[0][l]-v1[1][l] == v2[0][l]-v2[1][l] {
// 			commonPointP1 = append(commonPointP1, v1[0], v1[1])
// 			commonPointP2 = append(commonPointP2, v2[1], v2[0])
// 		} else {
// 			commonPointP1 = append(commonPointP1, v1[0], v1[1])
// 			commonPointP2 = append(commonPointP2, v2[0], v2[1])
// 		}

// 		for _, v := range v1 {
// 			key := fmt.Sprintf("%d,%d,%d", v[0], v[1], v[2])
// 			if _, ok := p1Found[key]; ok {
// 				continue
// 			}
// 			p1Found[key] = true
// 		}
// 	}

// 	if len(p1Found) > 0 && debug {
// 		fmt.Println("cp1", commonPointP1)
// 		fmt.Println("cp2", commonPointP2)
// 	}

// 	if len(p1Found) <= 11 {
// 		return nil, nil
// 	}

// 	return commonPointP1, commonPointP2
// }

// func getP2ScannerFromP1(p1positions, p2positions [][]int) *scanner {
// 	opsNegative := []func(int, int, int) (int, int, int){
// 		func(i1, i2, i3 int) (int, int, int) {
// 			return i1, i2, i3
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return -i1, i2, i3
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return i1, -i2, i3
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return i1, i2, -i3
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return -i1, -i2, i3
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return -i1, i2, -i3
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return i1, -i2, -i3
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return -i1, -i2, -i3
// 		},
// 	}
// 	opsRotate := []func(int, int, int) (int, int, int){
// 		func(i1, i2, i3 int) (int, int, int) {
// 			return i1, i2, i3
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return i2, i3, i1
// 		}, func(i1, i2, i3 int) (int, int, int) {
// 			return i3, i1, i2
// 		},
// 	}

// 	p1diffs := getReadingDiffs(p1positions)
// 	for _, opNegative := range opsNegative {
// 		for _, opRotate := range opsRotate {
// 			newP2 := [][]int{}
// 			for _, v := range p2positions {
// 				v0, v1, v2 := opNegative(opRotate(v[0], v[1], v[2]))
// 				newP2 = append(newP2, []int{v0, v1, v2})
// 			}

// 			cp1, cp2 := getCommonDiffs(p1diffs, getReadingDiffs(newP2))
// 			if cp1 == nil {
// 				continue
// 			}

// 			correct := true
// 			x, y, z := cp1[0][0]+cp2[0][0], cp1[0][1]+cp2[0][1], cp1[0][2]+cp2[0][2]
// 			for i := 1; i < len(cp2); i++ {
// 				newX := cp1[i][0] + cp2[i][0]
// 				newY := cp1[i][1] + cp2[i][1]
// 				newZ := cp1[i][2] + +cp2[i][2]
// 				if newX != x || newY != y || newZ != z {
// 					correct = false
// 				}
// 			}

// 			if !correct {
// 				continue
// 			}

// 			return &scanner{
// 				x:          x,
// 				y:          y,
// 				z:          z,
// 				opNegative: opNegative,
// 				opRotate:   opRotate,
// 			}
// 		}
// 	}

// 	return nil
// }

// type scanner struct {
// 	x, y, z                        int
// 	opNegative, opRotate           func(int, int, int) (int, int, int)
// 	opNegativeIndex, opRotateIndex int
// }

// func getNumberOfBeacons(scannerReads map[int][][]int) int {
// 	scanners := map[int]*scanner{
// 		0: {
// 			x:          0,
// 			y:          0,
// 			z:          0,
// 			opNegative: func(i1, i2, i3 int) (int, int, int) { return i1, i2, i3 },
// 			opRotate:   func(i1, i2, i3 int) (int, int, int) { return i1, i2, i3 },
// 		},
// 	}

// 	// for len(scanners) < 4 {
// 	for len(scanners) < len(scannerReads) {
// 		for searchingScannerI := 0; searchingScannerI < len(scannerReads); searchingScannerI++ {
// 			if _, ok := scanners[searchingScannerI]; ok {
// 				continue
// 			}

// 			found := false
// 			for knownScannerI := range scanners {
// 				if searchingScannerI == knownScannerI {
// 					continue
// 				}
// 				if found {
// 					break
// 				}

// 				s := getP2ScannerFromP1(scannerReads[knownScannerI], scannerReads[searchingScannerI])
// 				if s == nil {
// 					continue
// 				}

// 				found = true
// 				newScannerReads := [][]int{}
// 				for _, v := range scannerReads[searchingScannerI] {
// 					x, y, z := s.opNegative(s.opRotate(v[0], v[1], v[2]))
// 					newScannerReads = append(newScannerReads, []int{s.x - x, s.y - y, s.z - z})
// 				}
// 				scannerReads[searchingScannerI] = newScannerReads
// 				scanners[searchingScannerI] = s

// 				fmt.Printf("Founds scanner %d using scanner %d\n", searchingScannerI, knownScannerI)
// 				fmt.Printf("Scanner pos %d,%d,%d\n", s.x, s.y, s.z)
// 				fmt.Printf("Scanner rotations neg:%d, rot%d\n", s.opNegativeIndex, s.opRotateIndex)
// 				fmt.Println()
// 			}
// 		}
// 	}

// 	// fmt.Println(0)
// 	// for _, v := range scannerReads[0] {
// 	// 	fmt.Println(v)
// 	// }
// 	// fmt.Println("444")
// 	// for _, v := range scannerReads[4] {
// 	// 	fmt.Printf("%d,%d,%d\n", v[0], v[1], v[2])
// 	// }
// 	fmt.Println("---------------------------------------------")
// 	fmt.Println("---------------------------------------------")
// 	fmt.Println("---------------------------------------------")
// 	fmt.Println("---------------------------------------------")
// 	fmt.Println("---------------------------------------------")
// 	debug = true
// 	s2 := getP2ScannerFromP1(scannerReads[0], scannerReads[2])
// 	fmt.Println(s2)
// 	s2 = getP2ScannerFromP1(scannerReads[1], scannerReads[2])
// 	fmt.Println(s2)
// 	s2 = getP2ScannerFromP1(scannerReads[3], scannerReads[2])
// 	fmt.Println(s2)
// 	s2 = getP2ScannerFromP1(scannerReads[4], scannerReads[2])
// 	fmt.Println(s2)

// 	return 0
// }

// // Day19 implements day two challenge
// func Day19() error {
// 	scannerReads := getInputDay19()

// 	getNumberOfBeacons(scannerReads)

// 	return nil
// }

// // returns map key -> scanner, []int tuples of x y z scan values
// func getInputDay19() map[int][][]int {
// 	currentScanner := -1
// 	f, err := os.Open("inputs/19.txt")
// 	if err != nil {
// 		panic(err)
// 	}
// 	defer f.Close()

// 	scans := map[int][][]int{}
// 	scanner := bufio.NewScanner(f)
// 	for scanner.Scan() {
// 		stringRow := scanner.Text()
// 		if strings.Contains(stringRow, "scanner") {
// 			currentScanner++
// 			continue
// 		}
// 		if stringRow == "" {
// 			continue
// 		}

// 		parts := strings.Split(stringRow, ",")
// 		readings := []int{}
// 		for _, stringValue := range parts {
// 			v, err := strconv.Atoi(stringValue)
// 			if err != nil {
// 				panic(err)
// 			}

// 			readings = append(readings, v)
// 		}

// 		scans[currentScanner] = append(scans[currentScanner], readings)
// 	}

// 	err = scanner.Err()
// 	if err != nil {
// 		panic(err)
// 	}

// 	return scans
// }
