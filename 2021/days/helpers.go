package days

import (
	"fmt"
)

func printMatrix(arr [][]int) {
	for i := range arr {
		for j := range arr[0] {
			fmt.Printf("%d", arr[i][j])
		}
		fmt.Println()
	}

	fmt.Println()
}

func panicOnError(err error) {
	if err != nil {
		panic(err)
	}
}
