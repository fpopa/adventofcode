package days

import (
	"fmt"
	"strconv"
)

const (
	literalValueType = 4

	totalLengthBitsMode             = '0'
	numberOfBitsForSubPacketsNumber = 15

	numberOfSubPacketsMode            = '1'
	numberOfBitsForNumberOfSubpackets = 11
)

type packet struct {
	name    string
	parent  string
	version int
	typeID  int

	// related to literal package
	value int

	// related to operator package
	mode                   rune
	numberOfBitsForPackets int
	numberOfPackets        int

	children []*packet
}

func hexToBinary(rawPacket string) []rune {
	binaryPacket := ""
	for _, s := range rawPacket {
		intPacket, err := strconv.ParseInt(string(s), 16, 0)
		panicOnError(err)

		binaryPacket += fmt.Sprintf("%04b", intPacket)
	}

	return []rune(binaryPacket)
}

// processLiteralValuePacket sets packet value and returns the number of bits used in processing.
func (p *packet) processLiteralValuePacket(packets []rune) int {
	bitsUsed := 0
	number := ""
	for packets[0] != '0' {
		number += string(packets[1:5])
		packets = packets[5:]
		bitsUsed += 5
	}
	number += string(packets[1:5])
	bitsUsed += 5

	v, err := strconv.ParseInt(string(number), 2, 0)
	panicOnError(err)

	p.value = int(v)

	return bitsUsed
}

func getIntFromBits(bits []rune, from, to int) int {
	v, err := strconv.ParseInt(string(bits[from:to]), 2, 0)
	panicOnError(err)
	return int(v)
}

func (p *packet) operatorType() bool {
	return p.typeID != literalValueType
}

// returns mode, number of packets
func (p *packet) processOperatorPacket(bits []rune) int {
	bitsUsed := 0

	p.mode = bits[0]
	bits = bits[1:]
	bitsUsed += 1

	switch p.mode {
	case totalLengthBitsMode:
		p.numberOfBitsForPackets = getIntFromBits(bits, 0, numberOfBitsForSubPacketsNumber)
		bitsUsed += numberOfBitsForSubPacketsNumber
	case numberOfSubPacketsMode:
		p.numberOfPackets = getIntFromBits(bits, 0, numberOfBitsForNumberOfSubpackets)
		bitsUsed += numberOfBitsForNumberOfSubpackets
	}

	return bitsUsed
}

var packetNumber = -1

const root string = "root"

func getName() string {
	packetNumber++
	if packetNumber == 0 {
		return root
	}
	return fmt.Sprintf("p%d", packetNumber)
}

// getFirstPacket returns first packet and number of bits it represents
func getFirstPacket(currentIndex int, rawBits []rune, parentName string) (*packet, int) {
	initialIndex := currentIndex

	version, err := strconv.ParseInt(string(rawBits[currentIndex:currentIndex+3]), 2, 0)
	currentIndex += 3
	panicOnError(err)
	typeID, err := strconv.ParseInt(string(rawBits[currentIndex:currentIndex+3]), 2, 0)
	currentIndex += 3
	panicOnError(err)

	p := &packet{
		name:     getName(),
		parent:   parentName,
		version:  int(version),
		typeID:   int(typeID),
		children: make([]*packet, 0),
	}

	switch typeID {
	case literalValueType:
		usedBits := p.processLiteralValuePacket(rawBits[currentIndex:])
		currentIndex += usedBits
	default:
		usedBits := p.processOperatorPacket(rawBits[currentIndex:])
		currentIndex += usedBits
	}

	return p, currentIndex - initialIndex
}

func solvePart1(packets []*packet) int {
	versionSum := 0

	for _, p := range packets {
		versionSum += p.version
	}

	return versionSum
}

func getPackets(p *packet, currentIndex int, rawBits []rune) ([]*packet, int) {
	if p == nil {
		p, newIndex := getFirstPacket(0, rawBits, "")

		newPackets, _ := getPackets(p, 0, rawBits[newIndex:])
		return append([]*packet{p}, newPackets...), 0
	}
	if p.typeID == literalValueType {
		return nil, 0
	}

	packets := []*packet{}
	loop := true

	initialIndex := currentIndex
	packetsFound := 0
	for loop {
		switch p.mode {
		case totalLengthBitsMode:
			if !(currentIndex-initialIndex < p.numberOfBitsForPackets) {
				loop = false
				continue
			}
		case numberOfSubPacketsMode:
			if !(packetsFound < p.numberOfPackets) {
				loop = false
				continue
			}
		}

		newPacket, bitsUsed := getFirstPacket(currentIndex, rawBits, p.name)
		packets = append(packets, newPacket)
		currentIndex += bitsUsed
		packetsFound++

		if newPacket.operatorType() {
			newPackets, bitsUsed := getPackets(newPacket, 0, rawBits[currentIndex:])
			currentIndex += bitsUsed
			packets = append(packets, newPackets...)
		}
	}

	return packets, currentIndex
}
func addChildren(packets []*packet) {
	packagesByName := map[string]*packet{}

	for _, p := range packets {
		packagesByName[p.name] = p
	}

	for _, p := range packets {
		parent, ok := packagesByName[p.parent]
		if !ok {
			continue
		}

		parent.children = append(parent.children, p)
	}
}

const (
	sum         int = 0
	product     int = 1
	minimum     int = 2
	maximum     int = 3
	value       int = 4
	greaterThan int = 5
	lessThan    int = 6
	equalTo     int = 7
)

func (p *packet) executeFunc(calc func(int, int) int) int {
	value := p.children[0].solve()
	for i := 1; i < len(p.children); i++ {
		value = calc(value, p.children[i].solve())
	}

	return value
}

func (p *packet) solve() int {
	switch p.typeID {
	case sum:
		return p.executeFunc(func(i1, i2 int) int { return i1 + i2 })
	case product:
		return p.executeFunc(func(i1, i2 int) int { return i1 * i2 })
	case minimum:
		return p.executeFunc(func(i1, i2 int) int {
			if i1 > i2 {
				return i2
			}

			return i1
		})
	case maximum:
		return p.executeFunc(func(i1, i2 int) int {
			if i1 < i2 {
				return i2
			}

			return i1
		})
	case value:
		return p.value
	case greaterThan:
		if p.children[0].solve() > p.children[1].solve() {
			return 1
		}
		return 0
	case lessThan:
		if p.children[0].solve() < p.children[1].solve() {
			return 1
		}
		return 0
	case equalTo:
		if p.children[0].solve() == p.children[1].solve() {
			return 1
		}
		return 0
	}

	return 0
}

func solvePart2(packets []*packet) int {
	addChildren(packets)

	return packets[0].solve()
}

// Day16 implements day two challenge
func Day16() error {
	packets, _ := getPackets(nil, 0, hexToBinary("005173980232D7F50C740109F3B9F3F0005425D36565F202012CAC0170004262EC658B0200FC3A8AB0EA5FF331201507003710004262243F8F600086C378B7152529CB4981400B202D04C00C0028048095070038C00B50028C00C50030805D3700240049210021C00810038400A400688C00C3003E605A4A19A62D3E741480261B00464C9E6A5DF3A455999C2430E0054FCBE7260084F4B37B2D60034325DE114B66A3A4012E4FFC62801069839983820061A60EE7526781E513C8050D00042E34C24898000844608F70E840198DD152262801D382460164D9BCE14CC20C179F17200812785261CE484E5D85801A59FDA64976DB504008665EB65E97C52DCAA82803B1264604D342040109E802B09E13CBC22B040154CBE53F8015796D8A4B6C50C01787B800974B413A5990400B8CA6008CE22D003992F9A2BCD421F2C9CA889802506B40159FEE0065C8A6FCF66004C695008E6F7D1693BDAEAD2993A9FEE790B62872001F54A0AC7F9B2C959535EFD4426E98CC864801029F0D935B3005E64CA8012F9AD9ACB84CC67BDBF7DF4A70086739D648BF396BFF603377389587C62211006470B68021895FCFBC249BCDF2C8200C1803D1F21DC273007E3A4148CA4008746F8630D840219B9B7C9DFFD2C9A8478CD3F9A4974401A99D65BA0BC716007FA7BFE8B6C933C8BD4A139005B1E00AC9760A73BA229A87520C017E007C679824EDC95B732C9FB04B007873BCCC94E789A18C8E399841627F6CF3C50A0174A6676199ABDA5F4F92E752E63C911ACC01793A6FB2B84D0020526FD26F6402334F935802200087C3D8DD0E0401A8CF0A23A100A0B294CCF671E00A0002110823D4231007A0D4198EC40181E802924D3272BE70BD3D4C8A100A613B6AFB7481668024200D4188C108C401D89716A080"))
	fmt.Println(solvePart1(packets))
	fmt.Println(solvePart2(packets))

	// solvePart1(hexToBinary("38006F45291200"))
	// solvePart1(hexToBinary("EE00D40C823060"))
	// solvePart1(hexToBinary("8A004A801A8002F478"))
	// solvePart1(hexToBinary("620080001611562C8802118E34"))
	// solvePart1(hexToBinary("C0015000016115A2E0802F182340"))
	// solvePart1(hexToBinary("A0016C880162017C3686B18A3D4780"))

	return nil
}
