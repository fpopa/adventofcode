package days

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getLeastRiskPath(mat [][]int) int {
	mat[0][0] = 0
	visiting := [][]int{{0, 0}}

	directions := [][]int{
		{-1, 0},
		{1, 0},
		{0, 1},
		{0, -1},
	}

	distancesCost := map[string]int{}
	distancesCost["0-0"] = 0
	for len(visiting) > 0 {
		i, j := visiting[0][0], visiting[0][1]
		distancetoIJ := distancesCost[fmt.Sprintf("%d-%d", i, j)]
		for _, dir := range directions {
			newI, newJ := i+dir[0], j+dir[1]

			// if point exceeds bounds continue
			if newI < 0 || newJ < 0 || newI >= len(mat) || newJ >= len(mat[0]) {
				continue
			}

			key := fmt.Sprintf("%d-%d", newI, newJ)
			currentDistanceToNewIJ, ok := distancesCost[key]

			alternativeDistanceToNewIJ := distancetoIJ + mat[newI][newJ]

			if !ok || currentDistanceToNewIJ > alternativeDistanceToNewIJ {
				distancesCost[key] = alternativeDistanceToNewIJ
				visiting = append(visiting, []int{newI, newJ})
			}
		}
		visiting = visiting[1:]
	}

	return distancesCost[fmt.Sprintf("%d-%d", len(mat)-1, len(mat[0])-1)]
}

func unfoldMatrix(mat [][]int, expand int) [][]int {
	var newMatrix = make([][]int, len(mat)*expand)
	for i := range newMatrix {
		newMatrix[i] = make([]int, len(mat[0])*expand)
	}

	initialMaxIndexI := len(mat)
	initialMaxIndexJ := len(mat[0])

	for i := range newMatrix {
		for j := range newMatrix[0] {
			repeat := i/initialMaxIndexI + j/initialMaxIndexJ
			oldI := i % initialMaxIndexI
			oldJ := j % initialMaxIndexJ

			newValue := mat[oldI][oldJ] + repeat
			if newValue > 9 {
				newValue = newValue%10 + 1
			}
			newMatrix[i][j] = newValue
		}
	}

	return newMatrix
}

// Day15 implements day two challenge
func Day15() error {
	m := getInputDay15()

	part1Output := getLeastRiskPath(unfoldMatrix(m, 1))
	fmt.Println(part1Output)

	part2Output := getLeastRiskPath(unfoldMatrix(m, 5))
	fmt.Println(part2Output)

	return nil
}

func getInputDay15() [][]int {
	f, err := os.Open("inputs/15.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	mat := [][]int{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		stringRow := scanner.Text()

		parts := strings.Split(stringRow, "")
		row := []int{}
		for _, stringValue := range parts {
			v, err := strconv.Atoi(stringValue)
			if err != nil {
				panic(err)
			}

			row = append(row, v)
		}
		mat = append(mat, row)
	}

	err = scanner.Err()
	if err != nil {
		panic(err)
	}

	return mat
}
