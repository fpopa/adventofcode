package days

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func valueLowerThanElement(value int, arr [][]int, i, j int) bool {
	if i < 0 || j < 0 {
		return true
	}

	if i >= len(arr) || j >= len(arr[0]) {
		return true
	}

	return value < arr[i][j]
}

func lowPoint(arr [][]int, i, j int) bool {
	directions := [][]int{
		{-1, 0},
		{1, 0},
		{0, -1},
		{0, 1},
	}

	for _, direction := range directions {
		dirI, dirJ := direction[0], direction[1]

		if !valueLowerThanElement(arr[i][j], arr, i+dirI, j+dirJ) {
			return false
		}
	}

	return true
}

func numberOfLowPoints(arr [][]int) int {
	sum := 0
	for i := range arr {
		for j := range arr[0] {
			if !lowPoint(arr, i, j) {
				continue
			}

			sum += 1 + arr[i][j]
		}
	}

	return sum
}

var visitedIndexes map[string]bool

func getBasinSize(arr [][]int, i, j int) int {
	if i < 0 || j < 0 || i == len(arr) || j == len(arr[0]) {
		return 0
	}
	if arr[i][j] == 9 {
		return 0
	}

	visitedKey := fmt.Sprintf("%d-%d", i, j)
	if _, ok := visitedIndexes[visitedKey]; ok {
		return 0
	}
	visitedIndexes[visitedKey] = true
	size := 1

	directions := [][]int{
		{-1, 0},
		{1, 0},
		{0, -1},
		{0, 1},
	}

	for _, direction := range directions {
		dirI, dirJ := direction[0], direction[1]

		if valueLowerThanElement(arr[i][j], arr, i+dirI, j+dirJ) {
			size += getBasinSize(arr, i+dirI, j+dirJ)
		}
	}

	return size
}

func sumOfBigBasins(arr [][]int) int {
	lowPoints := [][]int{}
	for i := range arr {
		for j := range arr[0] {
			if !lowPoint(arr, i, j) {
				continue
			}

			lowPoints = append(lowPoints, []int{i, j})
		}
	}

	basins := []int{}
	for _, point := range lowPoints {
		visitedIndexes = map[string]bool{}
		basins = append(basins, getBasinSize(arr, point[0], point[1]))
	}

	sort.Ints(basins)
	return basins[len(basins)-1] * basins[len(basins)-2] * basins[len(basins)-3]
}

// Day9 implements day two challenge
func Day9() error {
	arr, err := getInputDay9()
	if err != nil {
		return err
	}

	part1Output := numberOfLowPoints(arr)
	fmt.Printf("part1: %v\n", part1Output)

	part2Output := sumOfBigBasins(arr)
	fmt.Printf("part2: %v\n", part2Output)

	return nil
}

func getInputDay9() ([][]int, error) {
	f, err := os.Open("inputs/9.txt")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	arr := [][]int{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		stringRow := scanner.Text()

		parts := strings.Split(stringRow, "")
		row := []int{}
		for _, stringValue := range parts {
			v, err := strconv.Atoi(stringValue)
			if err != nil {
				return nil, err
			}

			row = append(row, v)
		}
		arr = append(arr, row)
	}

	err = scanner.Err()
	if err != nil {
		return nil, err
	}

	return arr, nil
}

// https://adventofcode.com/2021/day/9
/*
--- Day 9: Smoke Basin ---
These caves seem to be lava tubes. Parts are even still volcanically active; small hydrothermal vents release smoke into the caves that slowly settles like rain.

If you can model how the smoke flows through the caves, you might be able to avoid it and be that much safer. The submarine generates a heightmap of the floor of the nearby caves for you (your puzzle input).

Smoke flows to the lowest point of the area it's in. For example, consider the following heightmap:

2199943210
3987894921
9856789892
8767896789
9899965678
Each number corresponds to the height of a particular location, where 9 is the highest and 0 is the lowest a location can be.

Your first goal is to find the low points - the locations that are lower than any of its adjacent locations. Most locations have four adjacent locations (up, down, left, and right); locations on the edge or corner of the map have three or two adjacent locations, respectively. (Diagonal locations do not count as adjacent.)

In the above example, there are four low points, all highlighted: two are in the first row (a 1 and a 0), one is in the third row (a 5), and one is in the bottom row (also a 5). All other locations on the heightmap have some lower adjacent location, and so are not low points.

The risk level of a low point is 1 plus its height. In the above example, the risk levels of the low points are 2, 1, 6, and 6. The sum of the risk levels of all low points in the heightmap is therefore 15.

Find all of the low points on your heightmap. What is the sum of the risk levels of all low points on your heightmap?


--- Part Two ---
Next, you need to find the largest basins so you know what areas are most important to avoid.

A basin is all locations that eventually flow downward to a single low point. Therefore, every low point has a basin, although some basins are very small. Locations of height 9 do not count as being in any basin, and all other locations will always be part of exactly one basin.

The size of a basin is the number of locations within the basin, including the low point. The example above has four basins.

The top-left basin, size 3:

2199943210
3987894921
9856789892
8767896789
9899965678
The top-right basin, size 9:

2199943210
3987894921
9856789892
8767896789
9899965678
The middle basin, size 14:

2199943210
3987894921
9856789892
8767896789
9899965678
The bottom-right basin, size 9:

2199943210
3987894921
9856789892
8767896789
9899965678
Find the three largest basins and multiply their sizes together. In the above example, this is 9 * 14 * 9 = 1134.

What do you get if you multiply together the sizes of the three largest basins?


*/
