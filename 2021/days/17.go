package days

import (
	"fmt"
	"math"
)

// returns highest touched point, and number of distinct combinations of velocities.
func solve(initialX, initialY, xMin, xMax, yMax, yMin int) (int, int) {
	// compute values for x velocity values that would end up on between xMin and xMax
	xVelocities := []int{}
	for v := 1; v <= xMax; v++ {
		currentPosition := initialX
		currentVelocity := v

		for currentPosition <= xMax && currentVelocity != 0 {
			currentPosition += currentVelocity
			currentVelocity--

			if currentPosition >= xMin && currentPosition <= xMax {
				xVelocities = append(xVelocities, v)
				break
			}

		}
	}

	// compute values for y velocity values that would end up on between yMin and yMax
	maxVelocityY := int(math.Abs(float64(yMax))) - 1
	yVelocities := []int{}
	for v := yMax; v <= maxVelocityY; v++ {
		currentPosition := initialY
		currentVelocity := v

		for currentPosition >= yMax {
			currentPosition += currentVelocity
			currentVelocity--

			if currentPosition <= yMin {
				yVelocities = append(yVelocities, v)
				break
			}

		}
	}

	// check if velocity combination launch ends up in the target area
	// store combination and update maxReachedPositionY with max if
	// the combination is valid.
	velocityCombinations := map[string]bool{}
	maxReachedPositionY := 0
	for _, initialVelocityX := range xVelocities {
		for _, initialVelocityY := range yVelocities {
			vX := initialVelocityX
			vY := initialVelocityY
			yTempCurveMax := 0
			posX := 0
			posY := 0

			for posX <= xMax && posY >= yMax {
				if posY > yTempCurveMax {
					yTempCurveMax = posY
				}

				if posX >= xMin && posY <= yMin {
					velocityCombinations[fmt.Sprintf("%d,%d", initialVelocityX, initialVelocityY)] = true
					if yTempCurveMax > maxReachedPositionY {
						maxReachedPositionY = yTempCurveMax
					}
				}

				posX += vX
				posY += vY
				if vX != 0 {
					vX--
				}
				vY--
			}
		}
	}

	return maxReachedPositionY, len(velocityCombinations)
}

// Day17 implements day two challenge
func Day17() error {
	xMin, xMax, yMax, yMin := getInputDay17()
	highestPoint, distinctVelocities := solve(0, 0, xMin, xMax, yMax, yMin)
	fmt.Println("highestPoint", highestPoint)
	fmt.Println("distinctVelocities", distinctVelocities)

	return nil
}

func getInputDay17() (int, int, int, int) {
	// return 20, 30, -10, -5
	return 230, 283, -107, -57
}
