package days

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func findPaths(foundPaths map[string]bool, currentPath string, visitsRemaining map[string]int, edges map[string]map[string]bool, start, end string) {
	if start == end {
		foundPaths[currentPath] = true
		return
	}

	for point := range edges[start] {
		if visitsRemaining[point] == 0 {
			continue
		}

		newVisitsRemaining := map[string]int{}
		for k, v := range visitsRemaining {
			newVisitsRemaining[k] = v
		}
		newVisitsRemaining[point] -= 1
		findPaths(foundPaths, currentPath+","+point, newVisitsRemaining, edges, point, end)
	}
}

func getNumberOfPaths(currentPath string, visitsRemaining map[string]int, edges map[string]map[string]bool, start, end string) int {
	if start == end {
		return 1
	}

	found := 0
	for point := range edges[start] {
		if visitsRemaining[point] == 0 {
			continue
		}

		newVisitsRemaining := map[string]int{}
		for k, v := range visitsRemaining {
			newVisitsRemaining[k] = v
		}
		newVisitsRemaining[point] -= 1
		found += getNumberOfPaths(currentPath+","+point, newVisitsRemaining, edges, point, end)
	}

	return found
}

func getPathsWithVisitingSmallCaveTwice(currentPath string, caveSizes map[string]int, edges map[string]map[string]bool, start, end string) int {
	foundPaths := map[string]bool{}
	for k := range caveSizes {
		if k == "start" || k == "end" {
			continue
		}
		if k != strings.ToLower(k) {
			continue
		}

		newCaveSizes := map[string]int{}
		for kk, vv := range caveSizes {
			newCaveSizes[kk] = vv
		}
		newCaveSizes[k] = 2

		findPaths(foundPaths, currentPath, newCaveSizes, edges, start, end)
	}

	return len(foundPaths)
}

// Day12 implements day two challenge
func Day12() error {
	edges, caveSizes, err := getInputDay12()
	if err != nil {
		return err
	}

	caveSizes["start"] = 0
	part1Output := getNumberOfPaths("start", caveSizes, edges, "start", "end")
	fmt.Printf("part1: %v\n", part1Output)

	part2Output := getPathsWithVisitingSmallCaveTwice("start", caveSizes, edges, "start", "end")
	fmt.Printf("part2: %v\n", part2Output)

	return nil
}

func getCaveSize(s string) int {
	if s == "start" || s == "end" {
		return 1
	}

	if s == strings.ToLower(s) {
		return 1
	}

	return -1
}

func getInputDay12() (map[string]map[string]bool, map[string]int, error) {
	caveSizes := map[string]int{}
	edges := map[string]map[string]bool{}

	f, err := os.Open("inputs/12.txt")
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		stringRow := scanner.Text()

		parts := strings.Split(stringRow, "-")
		if _, ok := edges[parts[0]]; !ok {
			edges[parts[0]] = map[string]bool{}
		}
		edges[parts[0]][parts[1]] = true

		if _, ok := edges[parts[1]]; !ok {
			edges[parts[1]] = map[string]bool{}
		}
		edges[parts[1]][parts[0]] = true

		caveSizes[parts[0]] = getCaveSize(parts[0])
		caveSizes[parts[1]] = getCaveSize(parts[1])
	}

	err = scanner.Err()
	if err != nil {
		return nil, nil, err
	}

	return edges, caveSizes, nil
}

// https://adventofcode.com/2021/day/12
