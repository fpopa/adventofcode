package days

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func printMatrixWithStep(arr [][]int, step int) {
	fmt.Printf("Step: %d\n", step)
	printMatrix(arr)
}

func getFlashingOctopuses(arr [][]int, alreadyFlashed map[string]bool, flashingOctopuses [][]int) [][]int {
	directions := [][]int{
		{1, 0},
		{-1, 0},
		{0, 1},
		{0, -1},
		{-1, -1},
		{1, 1},
		{-1, 1},
		{1, -1},
	}

	newFlashingOctopuses := [][]int{}
	for _, f := range flashingOctopuses {
		i, j := f[0], f[1]
		key := fmt.Sprintf("%d-%d", i, j)

		if _, ok := alreadyFlashed[key]; ok {
			continue
		}
		arr[i][j] = 0
		alreadyFlashed[key] = true

		for _, direction := range directions {
			newI := i + direction[0]
			newJ := j + direction[1]
			// outside bounds
			if newI < 0 || newJ < 0 || newI == len(arr) || newJ == len(arr[0]) {
				continue
			}

			newKey := fmt.Sprintf("%d-%d", newI, newJ)
			if _, ok := alreadyFlashed[newKey]; ok {
				continue
			}

			arr[newI][newJ] += 1
			if arr[newI][newJ] > 9 {
				newFlashingOctopuses = append(newFlashingOctopuses, []int{newI, newJ})
			}
		}
	}

	return newFlashingOctopuses
}

func getFlashes(arr [][]int) int {
	flashes := 0
	steps := 100

	for s := 0; s < steps; s++ {
		flashingOctopuses := [][]int{}
		for i := range arr {
			for j := range arr[0] {
				arr[i][j]++
				if arr[i][j] > 9 {
					flashingOctopuses = append(flashingOctopuses, []int{i, j})
				}
			}
		}

		// alreadyFlashed keeps count of octopuses that already flashed and should not get more energy
		alreadyFlashed := map[string]bool{}
		for len(flashingOctopuses) > 0 {
			flashingOctopuses = getFlashingOctopuses(arr, alreadyFlashed, flashingOctopuses)
		}

		flashes += len(alreadyFlashed)
		// printMatrixWithStep(arr, s+1)
		// fmt.Println("flashes", flashes)
	}

	return flashes
}

func getAllFlashingStep(arr [][]int) int {
	for s := 0; ; s++ {
		flashingOctopuses := [][]int{}
		for i := range arr {
			for j := range arr[0] {
				arr[i][j]++
				if arr[i][j] > 9 {
					flashingOctopuses = append(flashingOctopuses, []int{i, j})
				}
			}
		}

		// alreadyFlashed keeps count of octopuses that already flashed and should not get more energy
		alreadyFlashed := map[string]bool{}
		for len(flashingOctopuses) > 0 {
			flashingOctopuses = getFlashingOctopuses(arr, alreadyFlashed, flashingOctopuses)
		}

		if len(alreadyFlashed) == 100 {
			return s + 1
		}
	}
}

// Day11 implements day two challenge
func Day11() error {
	arr, err := getInputDay11()
	if err != nil {
		return err
	}

	// part1Output := getFlashes(arr)
	// fmt.Printf("part1: %v\n", part1Output)

	part2Output := getAllFlashingStep(arr)
	fmt.Printf("part2: %v\n", part2Output)

	return nil
}

func getInputDay11() ([][]int, error) {
	f, err := os.Open("inputs/11.txt")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	arr := [][]int{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		stringRow := scanner.Text()

		parts := strings.Split(stringRow, "")
		row := []int{}
		for _, stringValue := range parts {
			v, err := strconv.Atoi(stringValue)
			if err != nil {
				return nil, err
			}

			row = append(row, v)
		}
		arr = append(arr, row)
	}

	err = scanner.Err()
	if err != nil {
		return nil, err
	}

	return arr, nil
}

// https://adventofcode.com/2021/day/11
