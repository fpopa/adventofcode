package days

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// Day4 implements day two challenge
func Day4() error {
	randomNumbers, grids, err := getInputDay4()
	if err != nil {
		return err
	}
	part1Output := getWinningGridScore(randomNumbers, append([]grid{}, grids...))
	fmt.Printf("part1: %v\n", part1Output)

	part2Output := getLastWinningGridScore(randomNumbers, grids)
	fmt.Printf("part2: %v\n", part2Output)

	return nil
}

func getWinningGridScore(randomNumbers []int, grids []grid) int {
	for _, number := range randomNumbers {
		for i := range grids {
			found := grids[i].markNumber(number)
			if !found {
				continue
			}

			rowOrCol := grids[i].winningCondition()
			if rowOrCol == nil {
				continue
			}

			return grids[i].computeScore(number)
		}
	}

	return 0
}

func getLastWinningGridScore(randomNumbers []int, grids []grid) int {
	gridsWon := 0
	for _, number := range randomNumbers {
		for i := range grids {
			gridWinningState := grids[i].won
			found := grids[i].markNumber(number)
			if !found {
				continue
			}

			rowOrCol := grids[i].winningCondition()
			if rowOrCol == nil {
				continue
			}

			// if grid winning state didn't change we haven't found a winning condition for the last grid
			if gridWinningState == grids[i].won {
				continue
			}

			gridsWon += 1
			if gridsWon != len(grids) {
				continue
			}

			return grids[i].computeScore(number)
		}
	}

	return 0
}

type grid struct {
	m             [][]int
	markedColumns map[int]map[int]int
	markedRows    map[int]map[int]int
	won           bool
}

func (g *grid) computeScore(muliplyBy int) int {
	sum := 0

	for i := range g.m {
		for j := range g.m[0] {
			_, ok := g.markedRows[i]
			if !ok {
				sum += g.m[i][j]
				continue
			}

			_, ok = g.markedRows[i][j]
			if ok {
				continue
			}

			sum += g.m[i][j]
		}
	}
	return sum * muliplyBy
}

func (g *grid) winningCondition() map[int]int {
	for _, c := range g.markedColumns {
		if len(c) == 5 {
			g.won = true
			return c
		}
	}

	for _, c := range g.markedRows {
		if len(c) == 5 {
			g.won = true
			return c
		}
	}

	return nil
}

func (g *grid) markNumberColumnRow(row, col, number int) {
	if _, ok := g.markedColumns[col]; !ok {
		g.markedColumns[col] = make(map[int]int)
	}
	g.markedColumns[col][row] = number

	if _, ok := g.markedRows[row]; !ok {
		g.markedRows[row] = make(map[int]int)
	}
	g.markedRows[row][col] = number
}

// markNumber returns true if the number was marked at least once in the matrix
func (g *grid) markNumber(number int) bool {
	marked := false
	for i := range g.m {
		for j := range g.m[0] {
			if g.m[i][j] == number {
				g.markNumberColumnRow(i, j, number)
				marked = true
			}
		}
	}

	return marked
}

func getInputDay4() ([]int, []grid, error) {
	randomNumbers := []int{}
	grids := []grid{}
	f, err := os.Open("inputs/4.txt")
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	// first line
	scanner.Scan()
	l := scanner.Text()
	for _, numberString := range strings.Split(l, ",") {
		number, err := strconv.Atoi(numberString)
		if err != nil {
			return nil, nil, err
		}

		randomNumbers = append(randomNumbers, number)
	}
	// skip empty line between numbers and first grid.
	scanner.Scan()

	newEmptyGrid := func() grid {
		return grid{
			m:             [][]int{},
			markedColumns: make(map[int]map[int]int),
			markedRows:    make(map[int]map[int]int),
			won:           false,
		}
	}
	g := newEmptyGrid()
	for scanner.Scan() {
		row := scanner.Text()
		if row == "" {
			grids = append(grids, g)
			g = newEmptyGrid()
			continue
		}

		rowNumbers := []int{}
		for _, numberString := range strings.Split(row, " ") {
			if numberString == "" {
				continue
			}
			number, err := strconv.Atoi(numberString)
			if err != nil {
				return nil, nil, err
			}

			rowNumbers = append(rowNumbers, number)
		}
		g.m = append(g.m, rowNumbers)
	}
	// append last grid.
	grids = append(grids, g)

	err = scanner.Err()
	if err != nil {
		return nil, nil, err
	}

	return randomNumbers, grids, nil
}

// https://adventofcode.com/2021/day/4
/*
--- Day 4: Giant Squid ---
You're already almost 1.5km (almost a mile) below the surface of the ocean, already so deep that you can't see any sunlight. What you can see, however, is a giant squid that has attached itself to the outside of your submarine.

Maybe it wants to play bingo?

Bingo is played on a set of boards each consisting of a 5x5 grid of numbers. Numbers are chosen at random, and the chosen number is marked on all boards on which it appears. (Numbers may not appear on all boards.) If all numbers in any row or any column of a board are marked, that board wins. (Diagonals don't count.)

The submarine has a bingo subsystem to help passengers (currently, you and the giant squid) pass the time. It automatically generates a random order in which to draw numbers and a random set of boards (your puzzle input). For example:

7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
After the first five numbers are drawn (7, 4, 9, 5, and 11), there are no winners, but the boards are marked as follows (shown here adjacent to each other to save space):

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
After the next six numbers are drawn (17, 23, 2, 0, 14, and 21), there are still no winners:

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
Finally, 24 is drawn:

22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
 8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
 6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
 1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
At this point, the third board wins because it has at least one complete row or column of marked numbers (in this case, the entire top row is marked: 14 21 17 24 4).

The score of the winning board can now be calculated. Start by finding the sum of all unmarked numbers on that board; in this case, the sum is 188. Then, multiply that sum by the number that was just called when the board won, 24, to get the final score, 188 * 24 = 4512.

To guarantee victory against the giant squid, figure out which board will win first. What will your final score be if you choose that board?

--- Part Two ---
On the other hand, it might be wise to try a different strategy: let the giant squid win.

You aren't sure how many bingo boards a giant squid could play at once, so rather than waste time counting its arms, the safe thing to do is to figure out which board will win last and choose that one. That way, no matter which boards it picks, it will win for sure.

In the above example, the second board is the last to win, which happens after 13 is eventually called and its middle column is completely marked. If you were to keep playing until this point, the second board would have a sum of unmarked numbers equal to 148 for a final score of 148 * 13 = 1924.

Figure out which board will win last. Once it wins, what would its final score be?

*/
